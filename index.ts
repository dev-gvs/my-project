const myString = "Hello, World! 🌎"

const chars = [...myString]

chars.forEach((c, i) => {
    console.log(c, i)
})